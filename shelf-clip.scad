cell = 42;
block = 42*4;
wall = 4.2;
scale([cell*4, wall, cell])
cube(1);

translate([-cell/3, 0, 0])
scale([cell/3+1, cell, cell])
cube(1);
