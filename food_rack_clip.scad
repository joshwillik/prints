module crescent(thickness=0.5){
	difference(){
		scale([1, 1, 1]) cylinder($fn=50);
		translate([0, 0, -.5])
		scale([1*(1-thickness), 1*(1-thickness), 2])
		cylinder($fn=50);

		rotate([0, 0, 45*1.5])
		translate([0, 0, -0.5])
		scale([1, 1, 2])
		cube();
		rotate([0, 0, 45*0.5])
		translate([0, 0, -0.5])
		scale([1, 1, 2])
		cube();
	}
}

module peg(r=0.25){
	intersection(){
		translate([0, r*+sqrt(2)/2, r*+sqrt(2)/2])
		rotate([45, 0, 0])
		scale([r, r, sqrt(2)])
		translate([0, 0, -.5])
		cylinder($fn=50);

		scale([1, 1, 1])
		cube(center=true);
	}
}

union(){
	scale([2, 2, 4])
	translate([0, 0, -0.5])
	crescent(thickness=0.25);

	translate([0, -3, 0])
	scale([2, 2, 2])
	peg(r=.15);

}
