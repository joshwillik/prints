module cap(){
	inner_width = 1/sqrt(2);
	difference(){
		translate([0, 0, .5])
		rotate([0, 45, 0])
		cube([inner_width, 1, inner_width], center=true);
		cube([1, 2, 1], center=true);
	}
}
rotate([0, 45, ])
cap();
